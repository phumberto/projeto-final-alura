package br.com.alura.ceep.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.LAYOUT_MODO_LINEAR;

public class ListaNotaPreferences {

    private static final String LISTA_NOTA_PREFERENCES = "br.com.alura.ceep.preferences.ListaNotaPreferences";
    private static final String LAYOUT_DA_LISTA = "layoutLista";
    private Context context;

    public ListaNotaPreferences(Context context) {
        this.context = context;
    }

    public void salvaLayout(int layout) {
        SharedPreferences preferences = getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(LAYOUT_DA_LISTA, layout);
        editor.commit();
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(LISTA_NOTA_PREFERENCES, context.MODE_PRIVATE);
    }

    public int getLayout() {
        SharedPreferences preferences = getSharedPreferences();
        return preferences.getInt(LAYOUT_DA_LISTA, LAYOUT_MODO_LINEAR);
    }
}
