package br.com.alura.ceep.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.alura.ceep.R;
import br.com.alura.ceep.model.Nota;
import br.com.alura.ceep.ui.recyclerview.adapter.FormularioNotaSeletorCorAdapter;
import br.com.alura.ceep.ui.recyclerview.adapter.listener.FormularioNotaCorItemClickListener;

import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.CHAVE_NOTA;
import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.COR_BRANCO;
import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.POSICAO_INVALIDA;

public class FormularioNotaActivity extends AppCompatActivity {


    public static final String TITULO_APPBAR_INSERE = "Insere nota";
    public static final String TITULO_APPBAR_ALTERA = "Altera nota";
    private static final String TAG = "TESTE";
    private int posicaoRecibida = POSICAO_INVALIDA;
    private TextView titulo;
    private TextView descricao;
    private FormularioNotaSeletorCorAdapter adapter;
    private Nota notaDoFormulario;
    private String corEscolhida;
    private View backgroundFormulario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_nota);

        setTitle(TITULO_APPBAR_INSERE);
        inicializaCampos();

        Intent dadosRecebidos = getIntent();
        if (dadosRecebidos.hasExtra(CHAVE_NOTA)) {
            setTitle(TITULO_APPBAR_ALTERA);
            notaDoFormulario = (Nota) dadosRecebidos.getSerializableExtra(CHAVE_NOTA);
            preencheCampos();
        } else {
            notaDoFormulario = new Nota();
            notaDoFormulario.setCor(COR_BRANCO);
        }

        List<String> todasCores = populaCoresDoSeletor();
        configuraRecyclerView(todasCores);
    }

    private void inicializaCampos() {
        titulo = findViewById(R.id.formulario_nota_titulo);
        descricao = findViewById(R.id.formulario_nota_descricao);
        backgroundFormulario = findViewById(R.id.formulario_layout);
    }

    private void preencheCampos() {
        titulo.setText(notaDoFormulario.getTitulo());
        descricao.setText(notaDoFormulario.getDescricao());
        backgroundFormulario.setBackgroundColor(Color.parseColor(notaDoFormulario.getCor()));
    }

    @NonNull
    private List<String> populaCoresDoSeletor() {
        List<String> todasCores = new ArrayList<>();
        todasCores.add(NotaActivityConstantes.COR_AMARELO);
        todasCores.add(NotaActivityConstantes.COR_AZUL);
        todasCores.add(NotaActivityConstantes.COR_BRANCO);
        todasCores.add(NotaActivityConstantes.COR_VERMELHO);
        todasCores.add(NotaActivityConstantes.COR_VERDE);
        todasCores.add(NotaActivityConstantes.COR_AMARELO);
        todasCores.add(NotaActivityConstantes.COR_LILAS);
        todasCores.add(NotaActivityConstantes.COR_CINZA);
        todasCores.add(NotaActivityConstantes.COR_MARROM);
        todasCores.add(NotaActivityConstantes.COR_ROXO);
        return todasCores;
    }

    private void configuraRecyclerView(List<String> todasCores) {
        RecyclerView seletorCores = findViewById(R.id.formulario_nota_seletor_cor_recyclerview);
        configuraAdapter(todasCores, seletorCores);
    }

    private void configuraAdapter(List<String> todasCores, RecyclerView seletorCores) {
        adapter = new FormularioNotaSeletorCorAdapter(this, todasCores);
        seletorCores.setAdapter(adapter);
        adapter.setOnItemClickListener(new FormularioNotaCorItemClickListener() {
            @Override
            public void onItemClick(String cor, int posicao) {
                backgroundFormulario.setBackgroundColor(Color.parseColor(cor));
                notaDoFormulario.setCor(cor);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_formulario_nota_salva, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (ehMenuSalvaNota(item)) {
            criaNota();
            retornaNota();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean ehMenuSalvaNota(MenuItem item) {
        return item.getItemId() == R.id.menu_formulario_nota_ic_salva;
    }

    @NonNull
    private void criaNota() {
        notaDoFormulario.setTitulo(titulo.getText().toString());
        notaDoFormulario.setDescricao(descricao.getText().toString());
    }

    private void retornaNota() {
        Intent resultadoInsercao = new Intent();
        resultadoInsercao.putExtra(CHAVE_NOTA, notaDoFormulario);
        setResult(Activity.RESULT_OK, resultadoInsercao);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("Nota", notaDoFormulario);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        notaDoFormulario = (Nota) savedInstanceState.getSerializable("Nota");
    }
}
