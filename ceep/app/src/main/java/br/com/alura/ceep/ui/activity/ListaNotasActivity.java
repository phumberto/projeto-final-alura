package br.com.alura.ceep.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import br.com.alura.ceep.R;
import br.com.alura.ceep.dao.NotaDAO;
import br.com.alura.ceep.model.Nota;
import br.com.alura.ceep.preferences.ListaNotaPreferences;
import br.com.alura.ceep.ui.recyclerview.adapter.ListaNotasAdapter;
import br.com.alura.ceep.ui.recyclerview.adapter.listener.ListaNotasItemClickListener;
import br.com.alura.ceep.ui.recyclerview.helper.callback.NotaItemTouchHelperCallback;

import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.CHAVE_NOTA;
import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.CODIGO_REQUISICAO_ALTERA_NOTA;
import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.CODIGO_REQUISICAO_INSERE_NOTA;
import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.LAYOUT_MODO_GRID;
import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.LAYOUT_MODO_LINEAR;
import static br.com.alura.ceep.ui.activity.NotaActivityConstantes.POSICAO_INVALIDA;

public class ListaNotasActivity extends AppCompatActivity {


    public static final String TITULO_APPBAR = "Notas";
    private ListaNotasAdapter adapter;
    private RecyclerView listaNotas;
    private ListaNotaPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_notas);

        preferences = new ListaNotaPreferences(ListaNotasActivity.this);

        setTitle(TITULO_APPBAR);

        configuraBotaoInsereNota();
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<Nota> todasNotas = pegaTodasNotas();
        configuraRecyclerView(todasNotas);
    }

    private List<Nota> pegaTodasNotas() {
        NotaDAO dao = new NotaDAO(this);
        return dao.buscaNotas();
    }

    private void configuraRecyclerView(List<Nota> todasNotas) {
        listaNotas = findViewById(R.id.lista_notas_recyclerview);
        configuraAdapter(todasNotas, listaNotas);
        configuraItemTouchHelper(listaNotas);
    }

    private void configuraAdapter(List<Nota> todasNotas, RecyclerView listaNotas) {
        adapter = new ListaNotasAdapter(this, todasNotas);
        listaNotas.setAdapter(adapter);
        adapter.setOnItemClickListener(new ListaNotasItemClickListener() {
            @Override
            public void onItemClick(Nota nota) {
                vaiParaFormularioNotaActivityAltera(nota);
            }
        });
    }

    private void vaiParaFormularioNotaActivityAltera(Nota nota) {
        Intent abreFormularioComNota = new Intent(ListaNotasActivity.this, FormularioNotaActivity.class);
        abreFormularioComNota.putExtra(CHAVE_NOTA, nota);
        startActivityForResult(abreFormularioComNota, CODIGO_REQUISICAO_ALTERA_NOTA);
    }

    private void configuraBotaoInsereNota() {
        TextView botaoInsereNota = findViewById(R.id.lista_notas_insere_nota);
        botaoInsereNota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vaiParaFormularioNotaActivityInsere();
            }
        });
    }

    private void vaiParaFormularioNotaActivityInsere() {
        Intent iniciaFormularioNota = new Intent(ListaNotasActivity.this, FormularioNotaActivity.class);
        startActivityForResult(iniciaFormularioNota, CODIGO_REQUISICAO_INSERE_NOTA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (ehResultadoInsereNota(requestCode, data)) {
            if (resultadoOk(resultCode)) {
                Nota notaRecebida = (Nota) data.getSerializableExtra(CHAVE_NOTA);
                adiciona(notaRecebida);
            }
        }
        if (ehResultadoAlteraNota(requestCode, data)) {
            if (resultadoOk(resultCode)) {
                Nota notaRecebida = (Nota) data.getSerializableExtra(CHAVE_NOTA);
                if (ehPosicaoValida(notaRecebida.getPosicao())) {
                    altera(notaRecebida);
                }
            }
        }
    }

    private boolean ehResultadoInsereNota(int requestCode, Intent data) {
        return ehCodigoRequisicaoInsereNota(requestCode) && temNota(data);
    }

    private boolean ehCodigoRequisicaoInsereNota(int requestCode) {
        return requestCode == CODIGO_REQUISICAO_INSERE_NOTA;
    }

    private boolean ehResultadoAlteraNota(int requestCode, Intent data) {
        return ehCodigoRequisicaoAlteraNota(requestCode) && temNota(data);
    }

    private boolean ehCodigoRequisicaoAlteraNota(int requestCode) {
        return requestCode == CODIGO_REQUISICAO_ALTERA_NOTA;
    }

    private boolean temNota(Intent data) {
        return data != null && data.hasExtra(CHAVE_NOTA);
    }

    private boolean resultadoOk(int resultCode) {
        return resultCode == Activity.RESULT_OK;
    }

    private void adiciona(Nota nota) {
        nota = new NotaDAO(this).insere(nota);
        adapter.adiciona(nota);
    }

    private boolean ehPosicaoValida(int posicaoRecebida) {
        return posicaoRecebida > POSICAO_INVALIDA;
    }

    private void altera(Nota nota) {
        new NotaDAO(this).altera(nota);
        adapter.altera(nota);
    }

    private void configuraItemTouchHelper(RecyclerView listaNotas) {
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new NotaItemTouchHelperCallback(this, adapter));
        itemTouchHelper.attachToRecyclerView(listaNotas);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_lista_nota_layout, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (preferences.getLayout() == LAYOUT_MODO_LINEAR) {
            mudaLayoutParaLinear(menu);
        } else if (preferences.getLayout() == LAYOUT_MODO_GRID) {
            mudaLayoutParaGrid(menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void mudaLayoutParaGrid(Menu menu) {
        menu.findItem(R.id.menu_lista_nota_layout_grid).setVisible(false);
        menu.findItem(R.id.menu_lista_nota_layout_linear).setVisible(true);
        listaNotas.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));
    }

    private void mudaLayoutParaLinear(Menu menu) {
        menu.findItem(R.id.menu_lista_nota_layout_grid).setVisible(true);
        menu.findItem(R.id.menu_lista_nota_layout_linear).setVisible(false);
        listaNotas.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_lista_nota_layout_grid:
                preferences.salvaLayout(LAYOUT_MODO_GRID);
                invalidateOptionsMenu();
                return true;
            case R.id.menu_lista_nota_layout_linear:
                preferences.salvaLayout(LAYOUT_MODO_LINEAR);
                invalidateOptionsMenu();
                return true;
            case R.id.menu_lista_nota_feedback:
                vaiParaFormularioFeedback();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void vaiParaFormularioFeedback() {
        Intent iniciaFormularioFeedback = new Intent(ListaNotasActivity.this, FeedbackActivity.class);
        startActivity(iniciaFormularioFeedback);
    }
}
