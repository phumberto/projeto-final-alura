package br.com.alura.ceep.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class SplashScreenPreferences {

    public static final String SPLASH_SCREEN_PREFERENCES = "br.com.alura.ceep.preferences.SplashScreenPreferences";
    public static final String USUARIO_ACESSOU = "usuarioAcessou";
    private final Context context;

    public SplashScreenPreferences(Context context) {
        this.context = context;
    }

    public void setUsuarioAcessou() {
        SharedPreferences preferences = getSharedPreferences();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(USUARIO_ACESSOU, true);
        editor.commit();
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(SPLASH_SCREEN_PREFERENCES, context.MODE_PRIVATE);
    }

    public Boolean getUsuarioAcessou() {
        SharedPreferences preferences = getSharedPreferences();
        return preferences.getBoolean(USUARIO_ACESSOU, false);
    }
}
