package br.com.alura.ceep.ui.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import br.com.alura.ceep.R;
import br.com.alura.ceep.preferences.SplashScreenPreferences;

public class SplashScreenActivity extends AppCompatActivity {

    private SplashScreenPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        preferences = new SplashScreenPreferences(SplashScreenActivity.this);
        if (preferences.getUsuarioAcessou()){
            tempoDeEspera(500);
        } else {
            preferences.setUsuarioAcessou();
            tempoDeEspera(2000);
        }
    }

    private void tempoDeEspera(int tempoDeEspera) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                vaiParaListaDeNota();
            }
        }, tempoDeEspera);
    }

    private void vaiParaListaDeNota() {
        Intent intent = new Intent(SplashScreenActivity.this, ListaNotasActivity.class);
        startActivity(intent);
        finish();
    }
}
