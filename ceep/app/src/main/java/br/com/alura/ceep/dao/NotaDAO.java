package br.com.alura.ceep.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import br.com.alura.ceep.model.Nota;

public class NotaDAO extends SQLiteOpenHelper {

    private static final String TABELA = "Notas";

    public NotaDAO(@Nullable Context context) {
        super(context, "Ceep", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABELA + "(id INTEGER PRIMARY KEY AUTOINCREMENT, titulo TEXT, descricao TEXT, cor TEXT, posicao INT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Notas";
        db.execSQL(sql);
        onCreate(db);
    }

    public Nota insere(Nota nota) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = pegaDadosDaNota(nota);

        long idNovo = db.insert("Notas", null, dados);
        nota.setId(idNovo);
        nota.setPosicao(0);
        altera(nota);
        alteraPosicaoNotas(idNovo);
        return nota;
    }

    private void alteraPosicaoNotas(Long id) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE Notas SET posicao = (posicao + 1) WHERE id <> " + id;
        db.execSQL(sql);
    }

    private ContentValues pegaDadosDaNota(Nota nota) {
        ContentValues dados = new ContentValues();
        if (nota.getId() != null) {
            dados.put("id", nota.getId());
        }
        dados.put("titulo", nota.getTitulo());
        dados.put("descricao", nota.getDescricao());
        dados.put("cor", nota.getCor());
        dados.put("posicao", nota.getPosicao());
        return dados;
    }

    public List<Nota> buscaNotas() {
        String sql = "SELECT * FROM Notas ORDER BY posicao ASC";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(sql, null);
        List<Nota> notas = populaNotas(cursor);
        cursor.close();

        return notas;
    }

    private List<Nota> populaNotas(Cursor cursor) {
        List<Nota> notas = new ArrayList<Nota>();
        while (cursor.moveToNext()) {
            Nota nota = new Nota(cursor.getString(cursor.getColumnIndex("titulo")), cursor.getString(cursor.getColumnIndex("descricao")));
            nota.setId(cursor.getLong(cursor.getColumnIndex("id")));
            nota.setCor(cursor.getString(cursor.getColumnIndex("cor")));
            nota.setPosicao(cursor.getInt(cursor.getColumnIndex("posicao")));
            notas.add(nota);
        }
        return notas;
    }

    public void altera(Nota nota) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = pegaDadosDaNota(nota);
        String[] params = {nota.getId().toString()};
        db.update("Notas", dados, "id = ?", params);
    }

    public void remove(Nota nota) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABELA, "id=?", new String[]{String.valueOf(nota.getId())});
        removePosicaoNotas(nota);
    }

    private void removePosicaoNotas(Nota nota) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE Notas SET posicao = (posicao - 1) WHERE posicao > " + nota.getPosicao();
        db.execSQL(sql);
    }

    public Nota buscaNota(int posicao) {
        Nota nota = new Nota();
        SQLiteDatabase db = getReadableDatabase();
        String sql = "SELECT * FROM Notas WHERE posicao = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(posicao)});
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            nota.setId(cursor.getLong(cursor.getColumnIndex("id")));
            nota.setTitulo(cursor.getString(cursor.getColumnIndex("titulo")));
            nota.setDescricao(cursor.getString(cursor.getColumnIndex("descricao")));
            nota.setCor(cursor.getString(cursor.getColumnIndex("cor")));
            nota.setPosicao(cursor.getInt(cursor.getColumnIndex("posicao")));
        }
        cursor.close();
        return nota;
    }

    public void troca(Nota notaInicial, Nota notaFinal) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "UPDATE Notas SET posicao = " + notaFinal.getPosicao() + " WHERE id = " + notaInicial.getId();
        db.execSQL(sql);
        sql = "UPDATE Notas SET posicao = " + notaInicial.getPosicao() + " WHERE id = " + notaFinal.getId();
        db.execSQL(sql);
    }
}
