package br.com.alura.ceep.ui.recyclerview.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import br.com.alura.ceep.R;
import br.com.alura.ceep.ui.recyclerview.adapter.listener.FormularioNotaCorItemClickListener;

public class FormularioNotaSeletorCorAdapter extends RecyclerView.Adapter<FormularioNotaSeletorCorAdapter.CorViewHolder> {

    private final List<String> seletorCores;
    private Context context;
    private FormularioNotaCorItemClickListener onItemClickListener;

    public FormularioNotaSeletorCorAdapter(Context context, List<String> seletorCores) {
        this.context = context;
        this.seletorCores = seletorCores;
    }

    public void setOnItemClickListener(FormularioNotaCorItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public FormularioNotaSeletorCorAdapter.CorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewCriada = LayoutInflater.from(context).inflate(R.layout.item_seletor_cor_nota, parent, false);
        return new CorViewHolder(viewCriada);
    }

    @Override
    public void onBindViewHolder(FormularioNotaSeletorCorAdapter.CorViewHolder holder, int position) {
        String cor = seletorCores.get(position);
        holder.vincula(cor);
    }

    @Override
    public int getItemCount() {
        return seletorCores.size();
    }

    class CorViewHolder extends RecyclerView.ViewHolder {

        private final ImageView itemCor;
        private final Drawable drawable;
        private String cor;

        public CorViewHolder(View itemView) {
            super(itemView);
            itemCor = itemView.findViewById(R.id.item_cor);
            drawable = itemCor.getDrawable();
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(cor, getAdapterPosition());
                }
            });
        }

        public void vincula(String cor) {
            this.cor = cor;
            drawable.setColorFilter(Color.parseColor(cor), PorterDuff.Mode.SRC);
        }
    }

}
