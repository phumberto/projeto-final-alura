package br.com.alura.ceep.ui.activity;

public interface NotaActivityConstantes {

    String CHAVE_NOTA = "nota";
    int CODIGO_REQUISICAO_INSERE_NOTA = 1;
    int CODIGO_REQUISICAO_ALTERA_NOTA = 2;
    int POSICAO_INVALIDA = -1;
    String CHAVE_POSICAO = "posicao";
    int LAYOUT_MODO_GRID = 1;
    int LAYOUT_MODO_LINEAR = 2;
    String COR_AZUL = "#408EC9";
    String COR_BRANCO = "#FFFFFF";
    String COR_VERMELHO = "#EC2F4B";
    String COR_VERDE = "#9ACD32";
    String COR_AMARELO = "#F9F256";
    String COR_LILAS = "#F1CBFF";
    String COR_CINZA = "#D2D4DC";
    String COR_MARROM = "#A47C48";
    String COR_ROXO = "#BE29EC";


}
