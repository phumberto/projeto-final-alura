package br.com.alura.ceep.ui.recyclerview.adapter.listener;

public interface FormularioNotaCorItemClickListener {

    void onItemClick(String cor, int posicao);

}
